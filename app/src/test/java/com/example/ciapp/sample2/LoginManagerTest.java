package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("teStuser1", "Password");
    }

    @Test
    public void testLoginSuccess() throws InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("teStuser1", "Password");
        assertThat(user.getUsername(), is("teStuser1"));
        assertThat(user.getPassword(), is("Password"));
    }

    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("teStuser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws InvalidPasswordException, UserNotFoundException {
        User user = loginManager.login("Iniad", "Password");
    }

}